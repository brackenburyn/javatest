/* 
* HashMapForStrings.java
* Noah Brackenbury
* 5/13/15
*/

import java.util.*; //For comparable
public class HashMapForStrings
{
    //Nested class bucket which acts as linked list in my hashing code
    private class bucket
    {
        private class Node
        {
            private String key;
            private String value;
            private Node next;
            
            //Node constructor
            private Node(String keyGiven, String valueGiven)
            {
                key = keyGiven;
                value = valueGiven;
                next = null;
            }
        }
        
        private Node head;
        
        //bucket constructor
        public bucket()
        {
            head = null;
        }
            
        public void add(String key, String value)
        {
            Node added = new Node(key, value);
            if (head == null)
                head = added;
            else 
            {
                Node cur = head;
                while (cur.next != null) 
                    cur = cur.next;
                cur.next = added;
            }
        }
        
        public String get(String getting)
        {
            Node cur = head;
            while (cur != null){
                if (getting.equals(cur.key)) return cur.value ;
                else cur = cur.next; 
            }
            return null;
        }
    }   
    
    //instance variables for hash map
    private ArrayList<bucket> table;
    private bucket tempbucket;
    
    //Hash map constructor
    public HashMapForStrings()
    {
        table = new ArrayList<bucket>();
        for (int i = 0; i < 10; i++)
        {
            bucket walrus = new bucket();
            table.add(walrus);
        }
        tempbucket = null;
    }
    
    //Puts your key and value into the hash map
    public void put(String key, String value)
    {
        int spot = key.hashCode()%10;
        if (spot < 0) spot = -spot;
        table.get(spot).add(key, value);
    }
        
    //Gets the value (plot) of the key (movie title) you pass as a parameter
    public String get(String key)
    {
        int spot = key.hashCode()%10;
        if (spot < 0) spot = -spot;
        tempbucket = table.get(spot);
        return tempbucket.get(key);      
    }

    
    //Main method for testing put and get methods
    public static void main(String[] args)
    {
        HashMapForStrings hashbrowns = new HashMapForStrings();
        hashbrowns.put("Superman", "Really swoll guy");
        hashbrowns.put("Jimmy", "Average joe");
        hashbrowns.put("Ghostbusters", "Peeps fight ghosts");
        hashbrowns.put("Zebras in Space", "They in space");
        hashbrowns.put("Flash", "Fast boi");
        hashbrowns.put("Arrow", "The Green Arrow");
        hashbrowns.put("Fla", "Shorter fast boi");
        hashbrowns.put("Flagawagon", "Ride");
        hashbrowns.put("Flaaaa", "Like a sheep, but not quite");
        hashbrowns.put("Flask", "Drank");
        hashbrowns.put("Flag", "Symbol of our country");
        hashbrowns.put("Flagon", "Moar drank");
        hashbrowns.put("Flair", "Stylish");
        
        System.out.println(hashbrowns.get("Superman"));
        System.out.println(hashbrowns.get("Jimmy"));
        System.out.println(hashbrowns.get("bugs"));
        System.out.println(hashbrowns.get("Ghostbusters"));
        System.out.println(hashbrowns.get("Flash"));
        System.out.println(hashbrowns.get("Arrow"));
        System.out.println(hashbrowns.get("Flag"));
        System.out.println(hashbrowns.get("Flagon"));
        System.out.println(hashbrowns.get("Flaaaa"));
        System.out.println(hashbrowns.get("Flagawagon"));
        System.out.println(hashbrowns.get("Fla"));
        System.out.println(hashbrowns.get("Flask"));
        System.out.println(hashbrowns.get("Flair"));

        //System.out.println(hashbrowns.getKeysForPrefix("Fla"));
    }
}
