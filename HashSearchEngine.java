/* 
* SearchEngine.java
* Noah Brackenbury
* 5/14/15
*/

import java.util.*;
import java.io.*;
public class HashSearchEngine
{
    //Main function that takes all movies and places them into Hashing Code
    public static void main (String [] args) throws IOException 
    {
        Scanner scan = new Scanner(new File(args[0]), "ISO-8859-1").useDelimiter("\\s*-------------------------------------------------------------------------------\\s*");
        Scanner input = new Scanner(System.in);
        String search = "";
        String key = "";
        String value = "";
        HashMapForStrings potato = new HashMapForStrings();
        String line;
        int i = 0;
        
        //Reads through file and adds the movie titles and plots to the hash
        while (scan.hasNextLine())
        {
            //Code for movie title
            line = scan.nextLine();
            if (line.startsWith("MV:"))
            {
                key = line;
                key = key.replace("MV: ", "");
                line = scan.nextLine();
            }
            //Code for movie plots, only reads the first plot
            if (line.startsWith("PL:"))
            {
                while (line.startsWith("PL:"))
                {
                    line = line.replace("PL: ", "");
                    value = value + " " + line;
                    if (scan.hasNextLine())
                        line = scan.nextLine();
                }
                    //Adds to hash
                potato.put(key, value);
                value = "";
                if (scan.hasNext())
                    scan.next();
            }       
        }
        //Gets the user input for title and checks hash, or quits program when it is "####"
        while (!search.equals("####"))
        {
            System.out.println("What movie would you like to look for?");
            search = input.nextLine();
            if ((potato.get(search)) == null)
                System.out.println("No movies with this title!");
            else
                System.out.println(potato.get(search));
        }
    }
}
